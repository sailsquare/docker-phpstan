FROM phpstan/phpstan:0.11

ARG PHPSTAN_VERSION=0.11.4

RUN composer global remove phpstan/phpstan-shim && \
    composer global require \
        phpstan/phpstan:"$PHPSTAN_VERSION" \
        phpstan/phpstan-beberlei-assert \
        phpstan/phpstan-symfony \
        phpstan/phpstan-phpunit \
        phpstan/phpstan-doctrine \
    && \
    wget https://raw.githubusercontent.com/stefanotorresi/phpstan-symfony/fix-9-service-case-sensitivity/src/Symfony/ServiceMap.php \
        -O /composer/vendor/phpstan/phpstan-symfony/src/Symfony/ServiceMap.php
    # the things you have to do for love...
